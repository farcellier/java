package com.hibernate.jpa.standalone.dao;

import com.hibernate.jpa.standalone.entity.UtilisateurEntity;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by a092flam on 25/02/2015.
 */
public class UtilisateurDao {
	private EntityManager entityManager;

	public UtilisateurDao(EntityManager entityManager)
	{
		entityManager = entityManager;
	}

	public List<UtilisateurEntity> retrieveAll()
	{
		return this.entityManager.createQuery("from Utilisateur").getResultList();
	}

}

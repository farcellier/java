
package com.hibernate.jpa.standalone.entity;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "UTILISATEUR", schema = "EXT_COMMUN_PPS")
@Entity
public class UtilisateurEntity implements Serializable {

	@Id
	@Column(name = "ID_UTILISATEUR")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long idUtilisateur;

	@Column(name = "NOM")
	public String nom;
}

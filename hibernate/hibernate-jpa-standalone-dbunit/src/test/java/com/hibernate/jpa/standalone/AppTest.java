package com.hibernate.jpa.standalone;


import com.hibernate.jpa.standalone.entity.UtilisateurEntity;
import org.junit.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

/**
 * @author Geoffroy Warin (https://github.com/geowarin)
 */
public class AppTest {

	protected static EntityManagerFactory entityManagerFactory;
	protected static EntityManager entityManager;

	@BeforeClass
	public static void setUp() throws Exception {
		entityManagerFactory = Persistence.createEntityManagerFactory("persistence-test");
		entityManager = entityManagerFactory.createEntityManager();
	}

	@AfterClass
	public static void closeEntityManager() {
		entityManager.close();
		entityManagerFactory.close();
	}

	@Test
	@Ignore
	public void testInsert() {

		UtilisateurEntity newUtilisateur = new UtilisateurEntity();
		newUtilisateur.nom = "insert";


		entityManager.persist(newUtilisateur);
		long id = newUtilisateur.idUtilisateur;


		UtilisateurEntity utilisateur = entityManager.find(UtilisateurEntity.class, id);
		Assert.assertNotNull(utilisateur);
		Assert.assertEquals("insert", utilisateur.idUtilisateur);
	}

	@Test
	public void testFindAll() {
		List<UtilisateurEntity> allUtilisateurs = entityManager.createQuery("from UtilisateurEntity").getResultList();
		Assert.assertEquals(92, allUtilisateurs.size());
	}


}

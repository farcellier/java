package net.fabien_tools.hibernate_cascade.dao;

import net.fabien_tools.hibernate_cascade.entity.PersonEntity;

import javax.persistence.EntityManager;
import java.util.List;

public class PersonsDao {
    private EntityManager entityManager;

    public PersonsDao(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public List<PersonEntity> retrieveAll() {
        return this.entityManager.createQuery("from PersonEntity").getResultList();
    }

    public void save(PersonEntity personEntity) {
        this.entityManager.getTransaction().begin();
        this.entityManager.persist(personEntity);
        this.entityManager.getTransaction().commit();
    }
}

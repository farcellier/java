package net.fabien_tools.hibernate_cascade.entity;

import javax.persistence.*;

@Table(name = "families", schema = "public")
@Entity
public class FamilyEntity {

    @Id
    @Column(name = "id_families")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idFamilies;

    @Column(name= "adress")
    private String adress;

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }
}

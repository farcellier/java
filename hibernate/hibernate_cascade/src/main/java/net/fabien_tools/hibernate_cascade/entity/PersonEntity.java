package net.fabien_tools.hibernate_cascade.entity;

import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name = "persons", schema = "public")
@Entity
public class PersonEntity {
    @Id
    @Column(name = "id_persons")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idPersons;

    @ManyToOne()
    @JoinColumn(name = "id_families")
    private FamilyEntity family;

    @Column(name = "firstname")
    private String firstname;

    @Column(name = "surname")
    private String surname;

    public FamilyEntity getFamily() {
        return family;
    }

    public void setFamily(FamilyEntity family) {
        this.family = family;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
}

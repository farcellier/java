package net.fabien_tools.hibernate_cascade.dao;

import net.fabien_tools.hibernate_cascade.entity.PersonEntity;
import org.junit.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class PersonsDaoIntegrationTest {
    protected static EntityManagerFactory entityManagerFactory;
    protected static EntityManager entityManager;
    private PersonsDao tested;

    @BeforeClass
    public static void setUpClass() throws Exception {
        entityManagerFactory = Persistence.createEntityManagerFactory("persistence-test");
        entityManager = entityManagerFactory.createEntityManager();
    }

    @AfterClass
    public static void tearDownClass() {
        entityManager.close();
        entityManagerFactory.close();
    }

    @Before
    public void setUp() {
        this.tested = new PersonsDao(this.entityManager);
    }

    @Test
    public void retrieveAll_Given_PostgreDatabase_Then_Give4Records()
    {
        // Assign
        // Acts
        List<PersonEntity> persons = this.tested.retrieveAll();

        // Assert
        Assert.assertEquals(3, persons.size());
    }

    @Test
    public void save_Given_NoCascade_Then_FamilyPreserved()
    {
        // Assign
        List<PersonEntity> persons = this.tested.retrieveAll();

        // Acts
        persons.get(0).getFamily().setAdress("12 bd des champs elysees 75084 Paris");
        this.tested.save(persons.get(0));
        persons = this.tested.retrieveAll();

        // Assert
        Assert.assertEquals("47 rue des étoiles 75047 Paris", persons.get(0).getFamily().getAdress());
    }

}

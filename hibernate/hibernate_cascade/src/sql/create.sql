CREATE TABLE families (
    id_families integer PRIMARY KEY,
    adress varchar(256)
);

INSERT INTO families (id_families, adress) VALUES
    (1, "47 rue des étoiles 75047 Paris");

CREATE TABLE persons (
    id_persons integer PRIMARY KEY,
    id_families integer,
    firstname varchar(256),
    surname varchar(256)
);

INSERT INTO persons (id_persons, id_families, firstname, surname) VALUES
    (1, 1, "Jean", "Pleure"),
    (2, 1, "Marie", "Brizard"),
    (3, 1, "Mac", "Arena");
/**
 * Verifie que le or ou l'inclusive bitwise avec
 * des booleens, on obtient le même resultat.
 *
 */
public class InclusiveBitwise 
{
  public static void main( String[] args )
  {
    Boolean[] values = new Boolean[2];
    values[0] = false;
    values[1] = true;

    for(boolean value1 : values) {
      for(boolean value2 : values) {
        boolean or = value1 || value2;
        boolean inclusiveBitwise = value1 | value2;

        if(or != inclusiveBitwise) {
          System.out.println("Erreur de comparaison : or " + String.valueOf(or) + ", inclusiveBitwise " + String.valueOf(inclusiveBitwise));
        } else {
          System.out.println("Ok : or " + String.valueOf(or) + ", inclusiveBitwise " + String.valueOf(inclusiveBitwise));
        }
      }
    }
  }
}

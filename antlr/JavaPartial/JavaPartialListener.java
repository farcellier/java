// Generated from .\JavaPartial.g4 by ANTLR 4.5
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link JavaPartialParser}.
 */
public interface JavaPartialListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link JavaPartialParser#compilationUnit}.
	 * @param ctx the parse tree
	 */
	void enterCompilationUnit(JavaPartialParser.CompilationUnitContext ctx);
	/**
	 * Exit a parse tree produced by {@link JavaPartialParser#compilationUnit}.
	 * @param ctx the parse tree
	 */
	void exitCompilationUnit(JavaPartialParser.CompilationUnitContext ctx);
	/**
	 * Enter a parse tree produced by {@link JavaPartialParser#packageDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterPackageDeclaration(JavaPartialParser.PackageDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link JavaPartialParser#packageDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitPackageDeclaration(JavaPartialParser.PackageDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link JavaPartialParser#importDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterImportDeclaration(JavaPartialParser.ImportDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link JavaPartialParser#importDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitImportDeclaration(JavaPartialParser.ImportDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link JavaPartialParser#qualifiedName}.
	 * @param ctx the parse tree
	 */
	void enterQualifiedName(JavaPartialParser.QualifiedNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link JavaPartialParser#qualifiedName}.
	 * @param ctx the parse tree
	 */
	void exitQualifiedName(JavaPartialParser.QualifiedNameContext ctx);
}
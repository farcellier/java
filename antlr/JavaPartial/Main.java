package agefiph.migrations;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import agefiph.migrations.utils.ConnectionManager;
import agefiph.migrations.utils.JdbcHelper;

public class Main {

	/**
	 * .
	 * 
	 * @param args ?
	 */
	public static void main(String[] args) {
		Logger logger = Logger.getLogger(Main.class.getName());

		try {
			ConnectionManager manager = new ConnectionManager(logger);
			JdbcHelper jdbcHelper = new JdbcHelper();
			UtilisateurMigration utilisateurMigration = new UtilisateurMigration();
                        UtilisateurConflictMatcherService utilisateurConflictMatcher = new UtilisateurConflictMatcherService();
                        
			CommunUtilisateurDao communDao = new CommunUtilisateurDao(
                                manager.getCommunConnection(), logger, jdbcHelper);
			CommunPpsUtilisateurDao communPpsDao = new CommunPpsUtilisateurDao(
                                manager.getCommunPPSConnection(),logger, jdbcHelper);
			CommunUtilisateursNewDao communNewDao = new CommunUtilisateursNewDao(
                                manager.getCommunConnection(), jdbcHelper);
                        ConflictDao utilisateurConflictDao = new ConflictDao(
                                "UTILISATEURS_CONFLICTS",manager.getCommunConnection(), jdbcHelper);

			CommunPrestataireDao communPpsPrestataireDao = new CommunPrestataireDao(manager.getCommunPPSConnection(), jdbcHelper);
                        
			// Migration
			List<CommunUtilisateurDto> utilisateursFormation = communDao.getCommunUtilisateurs();
			List<CommunPpsUtilisateurDto> utilisateursPps = communPpsDao.getCommunPpsUtilisateurs();
			List<CommunUtilisateurNewDto> utilisateurs = utilisateurMigration.merge(utilisateursFormation,
					utilisateursPps);
			communNewDao.truncateTable();
			communNewDao.insertUtilisateurs(utilisateurs);

			List<CommunPrestataireDto> ppsPrestataires = communPpsPrestataireDao.readAll();
			
			// Recherche de conflits
			List<ConflictDto> conflicts = utilisateurConflictMatcher.getConflicts(utilisateurs);
                        utilisateurConflictDao.truncateTable();
                        utilisateurConflictDao.insertAll(conflicts);
                        
			manager.closeAll();
			
			logger.log(Level.INFO, "Migration effectuee");
		} catch (Exception ex) {
			logger.log(Level.SEVERE, ex.getMessage(), ex);
		}
	}
}

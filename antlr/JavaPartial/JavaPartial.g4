grammar JavaPartial;

// starting point for parsing a java file
compilationUnit
  :  packageDeclaration? importDeclaration* Any EOF
  ;

packageDeclaration
  :  'package' qualifiedName ';'
  ;

importDeclaration
  :  'import' qualifiedName ';'
  ;

// TOKENS
qualifiedName
    :   Identifier ('.' Identifier)*
    ;

Identifier
  :  Character+
  ;

fragment
Character
  : [a-zA-Z0-9$_]
  ;

fragment
Any
  : [^\s]*
  ;

WS  
  :  [ \t\r\n\u000C]+ -> skip
  ;
package net.fabientools.jdbc.postgresql_connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author FAR
 */
public class Main {

    public static void main(String[] arguments) {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, 
                    "Class org.postgresql.Driver does not exists", ex);
        }
        
        Connection db = null;
        try {
            db = DriverManager.getConnection("jdbc:postgresql://[::1]:5432/postgres",
                    "postgres",
                    "postgres");
            db.close();
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, 
                    "Fail to connect to database", ex);
        }
        
        System.out.print("Connection succeed");
    }
    
}

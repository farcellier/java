package junit.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Annotation used to define a test as a Slow Test.
 * 
 * @author Romain Linsolas
 * @version 1.0
 * @date 16/02/2011
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface SlowTest {

}

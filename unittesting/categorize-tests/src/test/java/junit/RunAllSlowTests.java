package junit;

import org.junit.runner.RunWith;

import junit.suite.SlowTestsSuite;

/**
 * Surefire - JUnit entry point. This class will run all test classes annotated with @FastTest.
 * 
 * @author Romain Linsolas
 * @version 1.0
 * @date 16/02/2011
 */
@RunWith(SlowTestsSuite.class)
public class RunAllSlowTests {

}

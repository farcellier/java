package junit.tests;

import static org.junit.Assert.fail;

import org.junit.Test;

/**
 * Test with no annotation. This test should not be launched in normal configuration.
 * 
 * @author Romain Linsolas
 * @version 1.0
 * @date 16/02/2011
 */
public class TestWithoutAnnotation {

	@Test
	public void aTestThatShouldNotBeLaunched() {
		fail("Noooooooooooo");
	}

}

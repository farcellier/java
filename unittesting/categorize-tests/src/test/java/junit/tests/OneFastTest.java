package junit.tests;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

import org.junit.Test;

import junit.annotation.FastTest;

/**
 * Test annotated with @FastTest. In normal configuration, this test is run when no profile is activated, or "jenkins" profile is enabled.
 * 
 * @author Romain Linsolas
 * @version 1.0
 * @date 16/02/2011
 */
@FastTest
public class OneFastTest {

	@Test
	public void oneTest() {
		System.out.println(" > One rapid test...");
		assertTrue(!false);
	}

	@Test
	public void anotherTest() {
		System.out.println(" > Another rapid test...");
		assertEquals(42, 42);
	}

}

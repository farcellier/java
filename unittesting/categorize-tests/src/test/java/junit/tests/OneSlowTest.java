package junit.tests;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.junit.Test;

import junit.annotation.SlowTest;

/**
 * Test annotated with @SlowTest. In normal configuration, this test is only run when the "jenkins" Maven profile is enabled (using -P argument, or
 * -Djenkins=true).
 * 
 * @author Romain Linsolas
 * @version 1.0
 * @date 16/02/2011
 */
@SlowTest
public class OneSlowTest {

	@Test
	public void oneTest() throws InterruptedException {
		System.out.println(" > One looooooooooong test...");
		TimeUnit.SECONDS.sleep(3);
		assertTrue(!false);
	}

	@Test
	public void anotherTest() throws InterruptedException {
		System.out.println(" > Another looooong test...");
		TimeUnit.SECONDS.sleep(3);
		assertEquals(42, 42);
	}

}

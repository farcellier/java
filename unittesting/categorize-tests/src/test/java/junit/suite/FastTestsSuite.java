package junit.suite;

import org.junit.runners.Suite;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.RunnerBuilder;

import junit.annotation.FastTest;
import junit.util.ClasspathClassesFinder;

/**
 * Suite that will search for all classes annotated by @FastTest.
 * 
 * @author Romain Linsolas
 * @version 1.0
 * @date 16/02/2011
 */
public class FastTestsSuite extends Suite {

	public FastTestsSuite(Class<?> clazz, RunnerBuilder builder) throws InitializationError {
		this(builder, clazz, ClasspathClassesFinder.getSuiteClasses("junit", FastTest.class));
	}

	public FastTestsSuite(RunnerBuilder builder, Class<?> clazz, Class<?>[] suiteClasses) throws InitializationError {
		super(builder, clazz, suiteClasses);
	}
}

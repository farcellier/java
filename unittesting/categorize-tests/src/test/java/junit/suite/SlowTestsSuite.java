package junit.suite;

import org.junit.runners.Suite;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.RunnerBuilder;

import junit.annotation.SlowTest;
import junit.util.ClasspathClassesFinder;

/**
 * Suite that will search for all classes annotated by @SlowTest.
 * 
 * @author Romain Linsolas
 * @version 1.0
 * @date 16/02/2011
 */
public class SlowTestsSuite extends Suite {

	public SlowTestsSuite(Class<?> clazz, RunnerBuilder builder) throws InitializationError {
		this(builder, clazz, ClasspathClassesFinder.getSuiteClasses("junit", SlowTest.class));
	}

	public SlowTestsSuite(RunnerBuilder builder, Class<?> clazz, Class<?>[] suiteClasses) throws InitializationError {
		super(builder, clazz, suiteClasses);
	}

}

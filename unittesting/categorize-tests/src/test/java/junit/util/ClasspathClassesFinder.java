package junit.util;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public final class ClasspathClassesFinder {

	/**
	 * Get the list of classes of a given package name, and that are annotated by a given annotation.
	 * 
	 * @param packageName The package name of the classes.
	 * @param testAnnotation The annotation the class should be annotated with.
	 * @return The List of classes that matches the requirements.
	 */
	public static Class<?>[] getSuiteClasses(String packageName, Class<? extends Annotation> testAnnotation) {
		try {
			return getClasses(packageName, testAnnotation);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Get the list of classes of a given package name, and that are annotated by a given annotation.
	 * 
	 * @param packageName The package name of the classes.
	 * @param annotation The annotation the class should be annotated with.
	 * @return The List of classes that matches the requirements.
	 * @throws ClassNotFoundException If something goes wrong...
	 * @throws IOException If something goes wrong...
	 */
	private static Class<?>[] getClasses(String packageName, Class<? extends Annotation> annotation) throws ClassNotFoundException, IOException {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		String path = packageName.replace('.', '/');
		// Get classpath
		Enumeration<URL> resources = classLoader.getResources(path);
		List<File> dirs = new ArrayList<File>();
		while (resources.hasMoreElements()) {
			URL resource = resources.nextElement();
			dirs.add(new File(resource.getFile()));
		}
		// For each classpath, get the classes.
		ArrayList<Class<?>> classes = new ArrayList<Class<?>>();
		for (File directory : dirs) {
			classes.addAll(findClasses(directory, packageName, annotation));
		}
		return classes.toArray(new Class[classes.size()]);
	}

	/**
	 * Find classes, in a given directory (recurively), for a given package name, that are annotated by a given annotation.
	 * 
	 * @param directory The directory where to look for.
	 * @param packageName The package name of the classes.
	 * @param annotation The annotation the class should be annotated with.
	 * @return The List of classes that matches the requirements.
	 * @throws ClassNotFoundException If something goes wrong...
	 */
	private static List<Class<?>> findClasses(File directory, String packageName, Class<? extends Annotation> annotation)
	        throws ClassNotFoundException {
		List<Class<?>> classes = new ArrayList<Class<?>>();
		if (!directory.exists()) {
			return classes;
		}
		File[] files = directory.listFiles();
		for (File file : files) {
			if (file.isDirectory()) {
				classes.addAll(findClasses(file, packageName + "." + file.getName(), annotation));
			} else if (file.getName().endsWith(".class")) {
				// We remove the .class at the end of the filename to get the class name...
				Class<?> clazz = Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6));
				// Does the file is annotated with the given annotation?
				if (clazz.getAnnotation(annotation) != null) {
					classes.add(clazz);
				}
			}
		}
		return classes;
	}

}
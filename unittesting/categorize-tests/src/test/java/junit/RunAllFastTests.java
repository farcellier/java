package junit;

import org.junit.runner.RunWith;

import junit.suite.FastTestsSuite;

/**
 * Surefire - JUnit entry point. This class will run all test classes annotated with @SlowTest.
 * 
 * @author Romain Linsolas
 * @version 1.0
 * @date 16/02/2011
 */
@RunWith(FastTestsSuite.class)
public class RunAllFastTests {

}
